<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeGetByEmail($query, $email)
    {
        return $query->where('email', $email)->first();
    }

    public function createUser($data)
    {
        $user = new User();
        $user->name = $data['name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        if (isset($data['social_id'])) {
            $user->social_id = $data['social_id'];
        } else {
            $user->password = Hash::make($data['password']);
        }
        if (isset($data['role'])) {
            $user->role_id = Role::where('name', $data['role'])->first()->id;
            $user->save();
        }
        else {
            $user->role_id = Role::where('name', 'User')->first()->id;
            $user->save();
        }
        return $user;

    }


    public function scopeFilterRole($query)
    {
        return $query->where('role_id', '2')->first();
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {

        return $this->role_id == 2;

    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
