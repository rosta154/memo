<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SocilLoginUserRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $currentUser = User::getByEmail($credentials['email']);
        return response()->json(['success' => true,'data' => $currentUser, 'token' => $token]);
    }

    public function socialLogin(SocilLoginUserRequest $request, User $users)
    {
        try {
            if ($user = User::where('social_id', $request->social_id)->first()) {
                $token = JWTAuth::fromUser($user);
            } else {
                $user = $users->createUser($request->all());
                $token = JWTAuth::fromUser($user);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(['success' => true, 'token' => $token, 'data' =>$user]);
    }

    public function adminLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $check = User::where('email', $credentials['email'])
            ->filterRole('Admin')
            ->first();
        if (!$check) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.notExist')]);
        }

        if (!Hash::check($credentials['password'], $check->password)) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.invalidPassword')]);
        }

        $user = User::getByEmail($credentials['email']);
        $token = null;
        try {
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['success' => false, 'message' => trans('messages.errors.cantFindAccount')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.failedLogin')], 500);
        }
        return response()->json(['success' => $user, 'token' => $token]);
    }
}
