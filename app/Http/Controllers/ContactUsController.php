<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\Http\Requests\ContactUsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
class ContactUsController extends Controller
{
    public function index()
    {
        return response()->json(['success' => true,'data' => ContactUs::all()]);
    }

    public function store(ContactUsRequest $request)
    {
        $contactUs = new ContactUs($request->all());

        Mail::send('mail.contactUs', compact('contactUs'), function ($message) use ($contactUs) {
            $message->to('rosta154@gmail.com')
                ->subject('Contact Us');
        });
        return response()->json(['success' => $contactUs->save(), 'data' => $contactUs]);
    }
}
