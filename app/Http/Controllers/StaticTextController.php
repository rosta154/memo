<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StaticText;
use App\Http\Requests\StaticTextDeleteRequest;
use App\Http\Requests\StaticTextRequest;
use App\Http\Requests\StaticTextUpdateRequest;
class StaticTextController extends Controller
{
    public function index()
    {
        $all = StaticText::all();
        return response()->json($all);
    }

    public function store(StaticTextRequest $request){

        $staticText = StaticText::create($request->all());

        return response()->json(['success' => true, 'data' => $staticText]);
    }

    public function update(StaticTextUpdateRequest $request){
        $credentials = $request->only(['name', 'description','description_1','title']);
        $staticText = StaticText::find($request->static_text_id);
        $staticText->update(['name' => $credentials['name'], 'description' => $credentials['description'], 'description_1' => $credentials['description_1'],'title' => $credentials['title']]);
        return response()->json(['success' => true,'data' => $staticText]);
    }

    public function destroy(StaticTextDeleteRequest $request){
        $credentials = $request->only('ids');
        StaticText::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => 'Static Text has been deleted']);
    }

    public function show(StaticText $staticText)
    {
        return response()->json(['success' => true,'data' => $staticText]);
    }
}
